-Dproduct.version=${product.version}
-Dproduct.data.version=${product.data.version}
-Dhttp.port.bak=${it.http.port}
-Drmi.port.bak=${it.rmi.port}
-DtestGroup=bar
-DskipAllPrompts=true
-Dallow.google.tracking=false
${invoker.product}:debug org.codehaus.gmaven:gmaven-plugin::execute